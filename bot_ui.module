<?php

/**
 * @file
 * Enables a user interface for sending messages on IRC through a bot.
 */

/**
 * Implementation of hook_help().
 */
function bot_ui_help($path, $arg) {
  switch ($path) {
    case 'irc:features':
      return array(t('UI'));
    case 'irc:features#ui':
      return t('It is possible to control the bot through a user interface at !url.', array('!url' => url('bot/ui', array('absolute' => TRUE))));
  }
}

/**
 * Implementation of hook_perm().
 */
function bot_ui_perm() {
  return array('send messages on irc');
}

/**
 * Implementation of hook_menu().
 */
function bot_ui_menu() {
  $items = array();

  $items['bot/ui'] = array(
    'title' => 'Bot (user interface)',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('bot_ui_page'),
    'access arguments' => array('send messages on irc'),
    'file' => 'bot_ui.pages.inc',
  );

  $items['bot/ui/js'] = array(
    'page callback' => 'bot_ui_js',
    'access arguments' => array('send messages on irc'),
    'file' => 'bot_ui.pages.inc',
    'type' => MENU_CALLBACK,
  );

  return $items;
}

/**
 * Implementation of hook_block().
 */
function bot_ui_block($op = 'list', $delta = 0, $edit = array()) {
  if ($op == 'list') {
    $blocks[0] = array(
      'info' => t('IRC channels'),
      'weight' => -2,
      'status' => 1,
      'region' => 'left',
      'cache' => BLOCK_CACHE_PER_PAGE,
    );

    $blocks[1] = array(
      'info' => t('IRC users'),
      'weight' => -1,
      'status' => 1,
      'region' => 'left',
      'cache' => BLOCK_CACHE_PER_PAGE,
    );

    return $blocks;
  }
  elseif ($op == 'view') {
    $block = array();
    $block['content'] = '';
    $block['subject'] = '';
    if (arg(0) == 'bot' && arg(1) == 'ui') {
      switch($delta) {
        case 0:
          $current_channel = arg(2);
          if (empty($current_channel)) {
            break;
          }
          $block['subject'] = t('IRC channels');
          $channels = preg_split('/\s*,\s*/', variable_get('bot_channels', '#test'));
          foreach ($channels as $key => $channel) {
            $parts = explode(' ', $channel);
            $channels[$key] = $parts[0];
            $links[] = l($parts[0], 'bot/ui/' . $parts[0]);
          }
          $block['content'] = theme('item_list', $links);
          break;

        case 1:
          $current_channel = arg(2);
          $channels = preg_split('/\s*,\s*/', variable_get('bot_channels', '#test'));
          foreach ($channels as $key => $channel) {
            $parts = explode(' ', $channel);
            $channels[$key] = $parts[0];
          }
          if (empty($current_channel) || !in_array($current_channel, $channels)) {
            break;
          }
          $block['subject'] = t('Users in @channel', array('@channel' => $current_channel));
          $items = array(t('Loading...'));
          $block['content'] = theme('item_list', $items, NULL, 'ul', array('id' => 'bot-ui-user-list'));
          break;
      }
    }
    return $block;
  }
}

/**
 * Create a new entry in the bot_ui log table.
 *
 * @param $type
 *   The type of message (as provided by Net_SmartIRC).
 * @param $channel
 *   The channel where this message took place (we ignore non-logged channels).
 * @param $nick
 *   (Optional) Who caused this message to occur.
 * @param $message
 *   The actual message itself.
 * @param $sent
 *   TRUE if the message has yet to be sent, FALSE otherwise.
 * @return $log_id
 *   The unique log ID for this entry; 0 otherwise.
 */
function bot_ui_log_insert($type, $channel, $nick, $message, $sent) {
  $log = new stdClass();
  $log->type      = $type;
  $log->timestamp = time();
  $log->channel   = $channel;
  $log->nick      = $nick;
  $log->message   = $message;
  $log->sent      = $sent;
  drupal_write_record('bot_ui_log', $log);
  return $log->id;
}

/**
 * Implementation of hook_irc_bot_cron().
 */
function bot_ui_irc_bot_cron() {
  static $nicks_loaded = FALSE;
  if (!$nicks_loaded) {
    db_query("DELETE FROM {bot_ui_nicks}");
    $channels = preg_split('/\s*,\s*/', variable_get('bot_channels', '#test'));
    foreach ($channels as $key => $channel) {
      $parts = explode(' ', $channel);
      $channels[$key] = $parts[0];
    }
    $GLOBALS['irc']->getList($channels);
    if (count($GLOBALS['irc']->_channels)) {
      $nicks_loaded = TRUE;
      foreach ($GLOBALS['irc']->_channels as $channel => $data) {
        $users = array_keys($data->users);
        foreach ($users as $user) {
          db_query("INSERT INTO {bot_ui_nicks} (channel, nick) VALUES ('%s', '%s')", $channel, $user);
        }
      }
    }
  }
  $result = db_query("SELECT * FROM {bot_ui_log} WHERE sent = 0 OR (sent = 1 AND nick = '%s')", $GLOBALS['irc']->_nick);
  while ($message = db_fetch_object($result)) {
    // For now, only actions and messages are supported.
    switch ($message->type) {
      case 256:
        bot_action($message->channel, $message->message);
        break;

      case 2:
        bot_message($message->channel, $message->message);
        break;
    }
    db_query('UPDATE {bot_ui_log} SET sent = 2 WHERE id = %d', $message->id);
  }
}

/**
 * Log regular channel communications.
 */
function bot_ui_irc_msg_channel($data) {
  bot_ui_log_insert($data->type, $data->channel, $data->nick, $data->message, TRUE);
}

/**
 * Log private messages.
 */
function bot_ui_irc_msg_query($data) {
  bot_ui_log_insert($data->tpe, $data->nick, $data->nick, $data->message, TRUE);
}

/**
 * Log a user joining a channel.
 */
function bot_ui_irc_msg_join($data) {
  db_query("INSERT INTO {bot_ui_nicks} (channel, nick) VALUES ('%s', '%s')", $data->channel, $data->nick);
  bot_ui_log_insert($data->type, $data->channel, $data->nick, $data->message, TRUE);
}

/**
 * Log a user leaving a channel.
 */
function bot_ui_irc_msg_part($data) {
  db_query("DELETE FROM {bot_ui_nicks} WHERE channel = '%s' AND nick = '%s'", $data->channel, $data->nick);
  bot_ui_log_insert($data->type, $data->channel, $data->nick, $data->message, TRUE);
}

/**
 * Log a user leaving the server.
 */
function bot_ui_irc_msg_quit($data) {
  $result = db_query("SELECT * FROM {bot_ui_nicks} WHERE nick = '%s'", $data->nick);
  while ($channel = db_fetch_array($result)) {
    bot_ui_log_insert($data->type, $channel['channel'], $data->nick, $data->message, TRUE);
  }
  db_query("DELETE FROM {bot_ui_nicks} WHERE nick = '%s'", $data->nick);
}

/**
 * Log a user performing an action.
 */
function bot_ui_irc_msg_action($data) {
  if ($data->channel == $GLOBALS['irc']->_nick) {
    $data->channel = $data->nick;
  }
  bot_ui_log_insert($data->type, $data->channel, $data->nick, $data->message, TRUE);
}