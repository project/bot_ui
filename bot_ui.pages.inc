<?php

/**
 * FAPI callback for the bot ui page.
 */
function bot_ui_page($form_state = NULL, $current_channel = '') {
  $form = array();
  $channels = preg_split('/\s*,\s*/', variable_get('bot_channels', '#test'));
  foreach ($channels as $key => $channel) {
    $parts = explode(' ', $channel);
    $channels[$key] = $parts[0];
  }
  if (empty($current_channel)) {
    if (count($channels) == 1) {
      $channel = $channels[0];
      drupal_goto('bot/ui/' . urlencode($channel));
    }
    $links = array();
    foreach ($channels as $channel) {
      $links[] = l($channel, 'bot/ui/' . $channel);
    }
    $form['options'] = array(
      '#type' => 'markup',
      '#prefix' => '<p>' . t('Please choose a channel from the list below.') . '</p>',
      '#value' => theme('item_list', $links),
    );
    return $form;
  }
  elseif ($current_channel == variable_get('bot_nickname', 'bot_module')) {
    $form['options'] = array(
      '#type' => 'markup',
      '#value' => t('You cannot private message yourself.'),
    );
    return $form;
  }
  drupal_add_css(drupal_get_path('module', 'bot_ui') .'/bot_ui.css');
  drupal_add_js(drupal_get_path('module', 'bot_ui') .'/bot_ui.js');
  $settings = array(
    'botName' => variable_get('bot_nickname', 'bot_module'),
    'botChannel' => $current_channel,
  );
  drupal_add_js($settings, 'setting');


  $form['console'] = array(
    '#type' => 'markup',
    '#prefix' => '<h3>' . check_plain($current_channel) . '</h3>',
    '#value' => '<div id="bot-ui-console">' . theme('table', array(t('Time'), t('User'), t('Message')), array()) . '</div>',
  );

  $form['send'] = array(
    '#prefix' => '<div class="container-inline">',
    '#suffix' => '</div>',
  );

  $form['send']['text'] = array(
    '#type' => 'textfield',
    '#maxlength' => 256,
  );

  $form['send']['send'] = array(
    '#type' => 'button',
    '#value' => t('Send'),
  );

  return $form;
}

/**
 * AJAX callback.
 */
function bot_ui_js() {
  $type = isset($_POST['type']) ? $_POST['type'] : '';
  switch ($type) {
    case 'send':
      $channel = $_POST['channel'];
      $nick = variable_get('bot_nickname', 'bot_module');
      $type = (preg_match('|^/me |', $_POST['message'])) ? 256 : 2;
      $message = ($type == 256) ? substr($_POST['message'], 4) : $_POST['message'];
      bot_ui_log_insert($type, $channel, $nick, $message, FALSE);
      drupal_json(array('data' => NULL, 'status' => 1));
      break;

    case 'receive':
      $data = array();
      $id = $_POST['id'];
      $channel = $_POST['channel'];
      if ($id != -1) {
        $result = db_query("SELECT * FROM {bot_ui_log} WHERE id > %d AND sent = 1 AND channel = '%s'", $id, $channel);
        while ($message = db_fetch_array($result)) {
          if ($message['type'] == 256) {
            $message['message'] = preg_replace('/^ACTION/', '', trim($message['message'], ''));
          }
          $data[] = $message;
        }
      }
      if ($_POST['refresh']) {
        $result = db_query("SELECT nick FROM {bot_ui_nicks} WHERE channel = '%s'", $channel);
        while ($nick = db_fetch_array($result)) {
          $nick_list[] = $nick['nick'];
        }
      }
      $id = max(db_result(db_query('SELECT MAX(id) FROM {bot_ui_log}')), 1);
      if (isset($nick_list)) {
        drupal_json(array('data' => $data, 'id' => $id, 'nicks' => $nick_list, 'status' => 1));
      }
      else {
        drupal_json(array('data' => $data, 'id' => $id, 'status' => 1));
      }
      break;

    default:
      drupal_json(array('data' => NULL, 'status' => 0));
      break;
  }
}