
Drupal.botUIid = -1;
Drupal.botUIpost = false;
Drupal.botUItr = 'even';
Drupal.botUIrefresh = 0;
Drupal.botUInicks = [];

Drupal.behaviors.botUITabAutocomplete = function() {
  $('#edit-text').keypress(function(e) {
    var start = $('#edit-text').val();
    if (e.keyCode == 9 && start.length > 0) {
      var matches = [];
      for (i in Drupal.botUInicks) {
        if (Drupal.botUInicks[i].indexOf(start) === 0) {
          matches[matches.length] = Drupal.botUInicks[i];
        }
      }
      if (matches.length == 1) {
        $('#edit-text').val(matches[0] + ': ');
      }
      return false;
    }
  });
};

Drupal.behaviors.botUISend = function() {
  $('#edit-send').click(function() {
    var message = $('#edit-text').val();
    $('#edit-text').val('');
    if (Drupal.botUIAddTableRowsSend(message)) {
      $.post(
        Drupal.settings.basePath + 'bot/ui/js',
        {
          type: 'send',
          channel: Drupal.settings.botChannel,
          message: message
        }
      );
    }
    return false;
  });
};

Drupal.behaviors.botUIReceive = function() {
  if (Drupal.botUIpost) {
    // Give ourselves some time to recover.
    Drupal.botUIpost = false;
    setTimeout('Drupal.behaviors.botUIReceive()', 10000);
    return;
  }
  Drupal.botUIpost = true;
  $.post(
    Drupal.settings.basePath + 'bot/ui/js',
    {
      type: 'receive',
      channel: Drupal.settings.botChannel,
      id: Drupal.botUIid,
      refresh: Number((Drupal.botUIrefresh % 30) == 0)
    },
    function(data) {
      Drupal.botUIpost = false;
      data = eval('(' + data + ')');
      if (data.status) {
        Drupal.botUIid = data.id;
        Drupal.botUIAddTableRowsReceive(data.data);
        if (typeof data.nicks != 'undefined') {
          Drupal.botUInicks = data.nicks;
          var html = '<ul class="item-list" id="bot-ui-user-list">';
          for (i in data.nicks) {
            html = html + '<li>';
            html = html + '<a href="' + Drupal.settings.basePath + Drupal.checkPlain(data.nicks[i]) + '">';
            html = html + Drupal.checkPlain(data.nicks[i]);
            html = html + '</a>';
            html = html + '</li>'
          }
          html = html + '</ul>';
          $('div#block-bot_ui-1 div.content').html(html);
        }
      }
    }
  );
  Drupal.botUIrefresh++;
  setTimeout('Drupal.behaviors.botUIReceive()', 2000);
};

Drupal.botUIAddTableRowsReceive = function(data) {
  for (message in data) {
    var type = data[message].type;
    var channel = data[message].channel;
    var date = new Date();
    date.setTime(data[message].timestamp * 1000);
    var nick = data[message].nick;
    var message = Drupal.checkPlain(data[message].message);
    Drupal.botUIThemeTableRow(type, channel, date, nick, message);
  }
};

Drupal.botUIAddTableRowsSend = function(message) {
  var type = 2;
  var channel = Drupal.settings.botChannel;
  if (message.charAt(0) == '/') {
    if (message.substr(0, 4) == '/me ') {
      type = 256;
      message = message.substr(4, message.length - 4);
    }
    else {
      type = -1;
      message = Drupal.t('Unknown command.');
    }
  }
  var nick = Drupal.settings.botName;
  var date = new Date();
  Drupal.botUIThemeTableRow(type, channel, date, nick, message);
  return (type != -1);
};

Drupal.botUIThemeTableRow = function(type, channel, date, nick, message) {
  var div = $('#bot-ui-console').get(0);
  var scroll = Math.abs(div.scrollHeight - div.scrollTop - 400) <= 20;
  var timestamp = '[' + ((date.getHours() < 10) ? ('0' + date.getHours()) : date.getHours()) + ':' + ((date.getMinutes() < 10) ? ('0' + date.getMinutes()) : date.getMinutes()) + ':' + ((date.getSeconds() < 10) ? ('0' + date.getSeconds()) : date.getSeconds()) + ']';
  var pinged = (message.indexOf(Drupal.settings.botName) != -1);
  var pingclass = (pinged ? ' bot-ui-ping' : '');
  channel = Drupal.checkPlain(channel);
  nick = Drupal.checkPlain(nick);
  Drupal.botUItr = (Drupal.botUItr == 'odd') ? 'even' : 'odd';
  type = Number(type);
  switch (type) {
    case -1:
      // Error.
      $('#bot-ui-console table').append('<tr class="bot-ui-error ' + Drupal.botUItr + '"><td>' + timestamp + '</td><td>&nbsp;</td><td>' + message + '</td></tr>');
      break;

    case 0:
    case 2:
      // Message.
      $('#bot-ui-console table').append('<tr class="bot-ui-message ' + Drupal.botUItr + pingclass + '"><td>' + timestamp + '</td><td>&lt;' + nick + '&gt;</td><td>' + message + '</td></tr>');
      break;

    case 64:
      // Join.
      $('#bot-ui-console table').append('<tr class="bot-ui-join ' + Drupal.botUItr + pingclass + '"><td>' + timestamp + '</td><td>&nbsp;</td><td>' + Drupal.t('!nick has joined !channel.', {'!nick': nick, '!channel': channel}) + '</td></tr>');
      break;

    case 256:
      // Action.
      $('#bot-ui-console table').append('<tr class="bot-ui-action ' + Drupal.botUItr + pingclass + '"><td>' + timestamp + '</td><td>&nbsp;</td><td>' + Drupal.t('***!nick !actions', {'!nick': nick, '!actions': message}) + '</td></tr>');
      break;

    case 4096:
      // Quit.
      $('#bot-ui-console table').append('<tr class="bot-ui-quit ' + Drupal.botUItr + pingclass + '"><td>' + timestamp + '</td><td>&nbsp;</td><td>' + Drupal.t('!nick has quit IRC (!quit_message).', {'!nick': nick, '!quit_message': message}) + '</td></tr>');
      break;

    case 524288:
      // Leave.
      $('#bot-ui-console table').append('<tr class="bot-ui-leave ' + Drupal.botUItr + pingclass + '"><td>' + timestamp + '</td><td>&nbsp;</td><td>' + Drupal.t('!nick has left !channel.', {'!nick': nick, '!channel': channel}) + '</td></tr>');
      break;

    default:
      Drupal.botUItr = (Drupal.botUItr == 'odd') ? 'even' : 'odd';
      break;
  }
  if (scroll) {
    div = $('#bot-ui-console').get(0);
    div.scrollTop = div.scrollHeight;
  }
};